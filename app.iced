
express = require "express"
app = express()
mongoose = require "mongoose"
passport = require "passport"
flash = require "connect-flash"

morgan = require "morgan"
cookieParser = require "cookie-parser"
bodyParser = require "body-parser"
session = require "express-session"
favicon = require "serve-favicon"
path = require "path"



configDB = require "./config/database"

# configuration

mongoose.connect configDB.url , ()->
	console.log "CONNECTED TO MONGODB at #{configDB.url}"



require("./config/passport")(passport)

# setup express application

app.use morgan('dev')
app.use cookieParser()
app.use bodyParser.json()
app.use bodyParser.urlencoded
    extended : false
app.use favicon('public/favicon.ico')

app.set 'view engine' , 'jade'

# passport settings

app.use session
    secret : "THISISREALLYGOOD"
    resave : false
    saveUninitialized: true

app.use passport.initialize()
app.use passport.session()
app.use flash()

app.use express.static( path.join( __dirname, "public"))


# routes
routes = require "./routes/routes"


routes app , passport

app.set 'port' , process.env.PORT || 8080

server = app.listen app.get('port') , ->
    console.log 'listening on ' + app.get('port')


module.exports = app
