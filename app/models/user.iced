

mongoose = require "mongoose"
bcrypt = require "bcrypt-nodejs"


userSchema = mongoose.Schema
  local     : 
    email     : String
    password  : String
    admin     : Boolean
    news      : Boolean
    faq       : Boolean
    



# METHODS

# generating a hash 

userSchema.methods.generateHash = ( password ) ->
  bcrypt.hashSync password , bcrypt.genSaltSync(8) , null


# Checking if a password is valid

userSchema.methods.validPassword  = ( password ) ->
  return bcrypt.compareSync password , this.local.password


# Create the model for users and expose it to out app

module.exports = mongoose.model "User" , userSchema