moment = require "moment"
News = require "../app/models/news"
FAQ = require "../app/models/faq"
mongoose = require "mongoose"

route = (app , passport)->

    # HOMEPAGE

    # news = [
    #   {
    #     "key": 1,
    #     "title": "কোডমার্শালে এনএইচএসপিসির প্রথম প্র্যাকটিস কনটেস্ট সফলভাবে আয়োজিত হয়েছে।"
    #     "description": "https://algo.codemarshal.org/contests/nhspcdemo1/"
    #     "date": "১৭ এপ্রিল ২০১৫"
    #   },
    #   {
    #     "key": 2,
    #     "title": "কোডমার্শালের ফোরাম চালু হয়েছে",
    #     "description": "https://discuss.codemarshal.org/"
    #     "date": "১৭ এপ্রিল ২০১৫"
    #   },
    #   {
    #     "key": 3,
    #     "title": "মাননীয় প্রতিমন্ত্রীর প্রেস কনফারেন্স",
    #     "description": "আগামী ২১ এপ্রিল মাননীয় প্রতিমন্ত্রী, আইসিটি বিভাগ বাংলাদেশ কম্পিউটার কাউন্সিলের অডিটরিয়ামে আনুষ্ঠানিকভাবে প্রতিযোগিতার উদ্বোধন করবেন।"
    #     "date": "১৭ এপ্রিল ২০১৫"
    #   },

    # ]

    app.get "/" , (req , res) ->
        res.render "home/home" , homeClass : "active"
    app.get "/about" , (req , res) ->
        res.render "about/about"

    app.get "/news" , (req , res) ->
        News.find {}   , (err , news)->
          if err? then res.send err

          news.reverse() 
          
          res.render "news/news" , news : news , newsClass : "active"

    app.get "/news-item/:id", (req , res) ->
        edit_class = "display : none ;"
        if req.isAuthenticated()
            edit_class = ""

        News.find {_id : mongoose.Types.ObjectId(req.params.id) } , (err , data)->
          res.render "news/news-page", headline: data[0].title , description : data[0].description , _id : req.params.id , editClass : edit_class

    app.get "/add-news", canEditNews , (req , res) ->
        res.render "newsAdd/add-news" , newsClass : "active"


    app.get "/bojipro" , (req, res) ->
        res.redirect "/faq"

    app.get "/time" , (req , res ) ->
        res.render "time-table/time-table" , timeClass : "active"

    app.get "/edit-news/:id", canEditNews , (req , res) ->
      News.find {_id : mongoose.Types.ObjectId(req.params.id) } , (err , data)->
        res.render "newsAdd/edit-news" , newsClass : "active" , n : data[0]


    app.post "/add-news", canEditNews ,(req , res) ->
        data = {
            'title' : req.body.headline ,
            'description' : req.body.description ,
            'date' : req.body.date ,
            'createdby' : req.user.email
        }

        news = new News()
        news.title = data.title
        news.description = data.description
        news.date = data.date
        news.createdby = data.createdby
              
        news.save (err)->
          if err then res.send err

          res.redirect "/news"

    app.post "/edit-news", canEditNews ,(req , res) ->
        data = {
            'title' : req.body.headline ,
            'description' : req.body.description ,
            'date' : req.body.date ,
            'createdby' : req.user.email
        }



        News.findOneAndUpdate {_id : mongoose.Types.ObjectId(req.body._id) } , data , (err)->
            if err then res.send err
            res.redirect "/news"


    app.get "/resources" , (req , res)->
        res.render "resources/resources" , resourcesClass : "active"

    app.get "/how-to" , (req , res) ->
      res.render "how-to/how-to" , howToClass : "active"

    app.get "/sample" , (req,res)->
      res.render "sample/sample" , sampleClass : "active"

    app.get "/login" , (req ,res) ->
      res.render "login/login"  , message : req.flash "loginMessage"

    app.post "/login" , passport.authenticate "local-login" , {
        successRedirect : "/"
        failureRedirect : "/login"
        failureFlash : true
    }

    app.get "/user-sign-up" , isAdmin , (req , res) ->
      res.render "login/sign-up" , message : req.flash "signupMessage"

    app.post "/user-sign-up" , passport.authenticate "local-signup" , {
        successRedirect: "/"
        failureRedirect : "/user-sign-up"
        failureFlash : true
    }

    app.get "/logout" , (req , res) ->
        req.logout()
        res.redirect "/"

    app.get "/contributors" , (req,res) ->
      res.render "contributors/contributors"    

    app.get "/add-faq", canEditNews , (req , res) ->
      res.render "faqAdd/add-faq" , faqClass : "active"

    app.post "/add-faq", canEditNews ,(req , res) ->
        faq = new FAQ()
        faq.question = req.body.question
        faq.answer = req.body.answer
        faq.save (err)->
          if err then res.send err

          res.redirect "/faq"

    app.get "/edit-faq/:id", canEditNews , (req , res) ->
      FAQ.find {_id : mongoose.Types.ObjectId(req.params.id) } , (err , data)->
        res.render "faqAdd/edit-faq" , faqClass : "active" , n : data[0]

    app.post "/edit-faq", canEditNews ,(req , res) ->
        data = {
            'question' : req.body.question ,
            'answer' : req.body.answer ,
        }

        FAQ.findOneAndUpdate {_id : mongoose.Types.ObjectId(req.body._id) } , data , (err)->
            if err then res.send err
            res.redirect "/faq"

    app.get "/faq" , (req, res) ->
      editS = "display:none;"
      if req.isAuthenticated()
        editS = ""
      FAQ.find {} ,(err, data) ->
        if err then res.send err
        res.render "bojipro/faq" , faqClass  : "active", faq : data , editStyle : editS

module.exports = route

###### Log In Checking ########

canEditNews = (req , res , next) ->

  # If Authenticated carry on


  if req.isAuthenticated()
    return next()

  # If not redirect to homepage

  res.redirect "/login"


isAdmin = (req , res , next) ->

  if req.isAuthenticated()
    return next()


  res.redirect "/"


canEditFAQFeed = (req , res , next) ->

  if req.isAuthenticated()
    return next()


  res.redirect "/"
