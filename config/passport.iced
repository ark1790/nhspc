

LocalStrategy = require("passport-local").Strategy

User = require "../app/models/user"


# Setup function

setupFunction = (passport) ->

  # PASSPORT SESSION SETUP

  passport.serializeUser (user, done) ->
    done null , user.id


  passport.deserializeUser (id , done) ->
    User.findById id , (err , user) ->
      done err , user 


  ######### LOCAL SIGNUP ################

  passport.use "local-signup" , new LocalStrategy {

    # By default local Strategy uses username and password, we will use override with email

      usernameField : 'email'
      passwordField : 'password'
      passReqToCallback : true  # Allows us to pass the entire request to the callback

    } , (req , email , password , done) ->

      # Check for user

      User.findOne { 'local.email' : email } , (err , user) ->
        if err? then return done err

        # Check if there is alreay a user

        if user?
          return done null , false , req.flash("signupMessage" , "That email is already taken.")

        # There is no user with that email

        # Create the user

        newUser = new User()

        newUser.local.email = email
        newUser.local.password = newUser.generateHash password
        newUser.local.admin = req.body.admin?
        newUser.local.news = req.body.news?
        newUser.local.faq = req.body.faq?

        # Save the User

        newUser.save (err) ->
          if err? then throw err

          done null , newUser



  # LOCAL LOGIN 

  passport.use "local-login" , new LocalStrategy {
      
      usernameField : 'email'
      passwordField : 'password'
      passReqToCallback : true

    } , (req , email , password , done) ->

      User.findOne { 'local.email' : email } , (err , user) ->

        if err? then return done err

        unless user?
          return done null , false , req.flash("loginMessage" , 'No user found.')

        # if user is found but the password wrong

        unless user.validPassword(password)
          return done null , false , req.flash('loginMessage' , "OOPS! Wrong Password.")

        done null , user # return the callback


module.exports = setupFunction